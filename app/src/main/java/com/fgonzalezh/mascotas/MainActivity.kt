package com.fgonzalezh.mascotas

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val recyclerView = findViewById<RecyclerView>(R.id.recycler_view)
        val animalAdapter = AnimalAdapter(createAnimals())

        recyclerView.layoutManager = LinearLayoutManager(this)
        recyclerView.adapter = animalAdapter

        animalAdapter.notifyDataSetChanged()
    }

    fun createAnimals(): List<Animal> {
        val animals = mutableListOf<Animal>()

        animals.add(
            Animal(
                    R.drawable.ic_gato,
            "Gato",
            "Los gatos son animales caseros que la gente cuida desde hace milenios"
            )
        )

        animals.add(
            Animal(
                R.drawable.ic_hamster,
                "Hamster",
                "Los hamster son mascotas que suelen dormir durante el dia y hacer actividad durante la noche"
            )
        )

        animals.add(
            Animal(
                R.drawable.ic_pez_payaso,
                "Pez Payaso",
                "Pescado que se volvió muy popular por la pelicula buscando a nemo"
            )
        )

        return animals
    }
}